package ch.neb.spacegame;

public interface CollisionListener {
	public void onCollide(GameEntity a, GameEntity b);
}
