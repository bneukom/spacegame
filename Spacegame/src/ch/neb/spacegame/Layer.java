package ch.neb.spacegame;

/**
 * Used to specify when a {@link GameEntity} has to be rendered.
 * 
 */
public enum Layer {
	DEFAULT, GAME_OVERLAY, HUD
}
