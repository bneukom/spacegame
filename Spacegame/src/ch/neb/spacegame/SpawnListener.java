package ch.neb.spacegame;

public interface SpawnListener {
	public void spawned(GameEntity entity);
}
